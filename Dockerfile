FROM alpine:latest

LABEL maintainer="Andrew Kozak <andrewkozak11@gmail.com>"
LABEL description="git-crypt in an alpine container"
LABEL url="https://gitlab.com/akozak1_lab/git-crypt-alpine/"

RUN echo https://dl-cdn.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    apk add --no-cache 'git-crypt=0.7.0-r0' 'kustomize=5.0.1-r1'

WORKDIR /

ENTRYPOINT ["sh"]
